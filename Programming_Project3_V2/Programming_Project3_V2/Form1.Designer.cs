﻿namespace Programming_Project3_V2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.firstWordAlphabeticallyLbl = new System.Windows.Forms.Label();
            this.lastWordAlphabeticallyLbl = new System.Windows.Forms.Label();
            this.longestWordLbl = new System.Windows.Forms.Label();
            this.mostVowelsLbl = new System.Windows.Forms.Label();
            this.pressBtnLbl = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.openFileBtn = new System.Windows.Forms.Button();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.firstWordAlphabeticallyTxtBx = new System.Windows.Forms.TextBox();
            this.lastWordAlphabeticallyTxtBx = new System.Windows.Forms.TextBox();
            this.longestWordTxtBx = new System.Windows.Forms.TextBox();
            this.wordWithMostVowelsTxtBx = new System.Windows.Forms.TextBox();
            this.numOfVowelsTxtBx = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // firstWordAlphabeticallyLbl
            // 
            this.firstWordAlphabeticallyLbl.AutoSize = true;
            this.firstWordAlphabeticallyLbl.Location = new System.Drawing.Point(277, 86);
            this.firstWordAlphabeticallyLbl.Name = "firstWordAlphabeticallyLbl";
            this.firstWordAlphabeticallyLbl.Size = new System.Drawing.Size(222, 20);
            this.firstWordAlphabeticallyLbl.TabIndex = 0;
            this.firstWordAlphabeticallyLbl.Text = "The first word alphabetically is:";
            // 
            // lastWordAlphabeticallyLbl
            // 
            this.lastWordAlphabeticallyLbl.AutoSize = true;
            this.lastWordAlphabeticallyLbl.Location = new System.Drawing.Point(277, 125);
            this.lastWordAlphabeticallyLbl.Name = "lastWordAlphabeticallyLbl";
            this.lastWordAlphabeticallyLbl.Size = new System.Drawing.Size(221, 20);
            this.lastWordAlphabeticallyLbl.TabIndex = 1;
            this.lastWordAlphabeticallyLbl.Text = "The last word alphabetically is:";
            // 
            // longestWordLbl
            // 
            this.longestWordLbl.AutoSize = true;
            this.longestWordLbl.Location = new System.Drawing.Point(277, 164);
            this.longestWordLbl.Name = "longestWordLbl";
            this.longestWordLbl.Size = new System.Drawing.Size(149, 20);
            this.longestWordLbl.TabIndex = 2;
            this.longestWordLbl.Text = "The longest word is:";
            // 
            // mostVowelsLbl
            // 
            this.mostVowelsLbl.AutoSize = true;
            this.mostVowelsLbl.Location = new System.Drawing.Point(277, 211);
            this.mostVowelsLbl.Name = "mostVowelsLbl";
            this.mostVowelsLbl.Size = new System.Drawing.Size(439, 20);
            this.mostVowelsLbl.TabIndex = 3;
            this.mostVowelsLbl.Text = "The number of vowels and the word with the most vowels are:";
            // 
            // pressBtnLbl
            // 
            this.pressBtnLbl.AutoSize = true;
            this.pressBtnLbl.Location = new System.Drawing.Point(34, 72);
            this.pressBtnLbl.Name = "pressBtnLbl";
            this.pressBtnLbl.Size = new System.Drawing.Size(187, 20);
            this.pressBtnLbl.TabIndex = 5;
            this.pressBtnLbl.Text = "Press this to Select a File";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // openFileBtn
            // 
            this.openFileBtn.Location = new System.Drawing.Point(60, 111);
            this.openFileBtn.Name = "openFileBtn";
            this.openFileBtn.Size = new System.Drawing.Size(141, 45);
            this.openFileBtn.TabIndex = 6;
            this.openFileBtn.Text = "Open";
            this.openFileBtn.UseVisualStyleBackColor = true;
            this.openFileBtn.Click += new System.EventHandler(this.OpenFileBtn_Click);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(0, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 328);
            this.splitter1.TabIndex = 7;
            this.splitter1.TabStop = false;
            // 
            // firstWordAlphabeticallyTxtBx
            // 
            this.firstWordAlphabeticallyTxtBx.Location = new System.Drawing.Point(734, 83);
            this.firstWordAlphabeticallyTxtBx.Name = "firstWordAlphabeticallyTxtBx";
            this.firstWordAlphabeticallyTxtBx.Size = new System.Drawing.Size(303, 26);
            this.firstWordAlphabeticallyTxtBx.TabIndex = 8;
            // 
            // lastWordAlphabeticallyTxtBx
            // 
            this.lastWordAlphabeticallyTxtBx.Location = new System.Drawing.Point(734, 122);
            this.lastWordAlphabeticallyTxtBx.Name = "lastWordAlphabeticallyTxtBx";
            this.lastWordAlphabeticallyTxtBx.Size = new System.Drawing.Size(303, 26);
            this.lastWordAlphabeticallyTxtBx.TabIndex = 9;
            // 
            // longestWordTxtBx
            // 
            this.longestWordTxtBx.Location = new System.Drawing.Point(734, 161);
            this.longestWordTxtBx.Name = "longestWordTxtBx";
            this.longestWordTxtBx.Size = new System.Drawing.Size(303, 26);
            this.longestWordTxtBx.TabIndex = 10;
            // 
            // wordWithMostVowelsTxtBx
            // 
            this.wordWithMostVowelsTxtBx.Location = new System.Drawing.Point(826, 208);
            this.wordWithMostVowelsTxtBx.Name = "wordWithMostVowelsTxtBx";
            this.wordWithMostVowelsTxtBx.Size = new System.Drawing.Size(211, 26);
            this.wordWithMostVowelsTxtBx.TabIndex = 11;
            // 
            // numOfVowelsTxtBx
            // 
            this.numOfVowelsTxtBx.Location = new System.Drawing.Point(734, 208);
            this.numOfVowelsTxtBx.Name = "numOfVowelsTxtBx";
            this.numOfVowelsTxtBx.Size = new System.Drawing.Size(67, 26);
            this.numOfVowelsTxtBx.TabIndex = 12;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1135, 328);
            this.Controls.Add(this.numOfVowelsTxtBx);
            this.Controls.Add(this.wordWithMostVowelsTxtBx);
            this.Controls.Add(this.longestWordTxtBx);
            this.Controls.Add(this.lastWordAlphabeticallyTxtBx);
            this.Controls.Add(this.firstWordAlphabeticallyTxtBx);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.openFileBtn);
            this.Controls.Add(this.pressBtnLbl);
            this.Controls.Add(this.mostVowelsLbl);
            this.Controls.Add(this.longestWordLbl);
            this.Controls.Add(this.lastWordAlphabeticallyLbl);
            this.Controls.Add(this.firstWordAlphabeticallyLbl);
            this.Name = "Form1";
            this.Text = "Get Statistics from a File";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label firstWordAlphabeticallyLbl;
        private System.Windows.Forms.Label lastWordAlphabeticallyLbl;
        private System.Windows.Forms.Label longestWordLbl;
        private System.Windows.Forms.Label mostVowelsLbl;
        private System.Windows.Forms.Label pressBtnLbl;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button openFileBtn;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.TextBox firstWordAlphabeticallyTxtBx;
        private System.Windows.Forms.TextBox lastWordAlphabeticallyTxtBx;
        private System.Windows.Forms.TextBox longestWordTxtBx;
        private System.Windows.Forms.TextBox wordWithMostVowelsTxtBx;
        private System.Windows.Forms.TextBox numOfVowelsTxtBx;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Programming_Project3_V2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        
        private void OpenFileBtn_Click(object sender, EventArgs e)
        {
            //I'll declare variables here that are neccesary for the txtBx returns.
            string textReadInFromFileInLongString;
            string textReadInLowerCase;
            string[] allWords;
            char[] wordThatHadTheMostVowels;

            //This is done in the style the book suggested
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //First read in the file as one long string through "using" 
                using (StreamReader fileReadIn = new StreamReader(openFileDialog1.FileName, Encoding.UTF8))
                {
                    //here the .ReadToEnd goes through the entire file.
                    textReadInFromFileInLongString = fileReadIn.ReadToEnd();
                    //taking in every piece of the string and turning it to lower case.
                    textReadInLowerCase = textReadInFromFileInLongString.ToLower();
                }//ends using

                //here we can send the long lower case string back to a file on one line
                StreamWriter lowerCaseWrite = File.CreateText("TheLowerCaseString.txt");
                lowerCaseWrite.WriteLine(textReadInLowerCase);
                lowerCaseWrite.Close();

                //allWords is an array that will be used to pass to the methods
                allWords = textReadInLowerCase.Split(' ');

                //Sending all results from methods straight to the text boxes
                firstWordAlphabeticallyTxtBx.Text = findFirstWordAlphabetically(allWords);
                lastWordAlphabeticallyTxtBx.Text = findLastWordAlphabetically(allWords);
                longestWordTxtBx.Text = findTheLongestWord(allWords);
                wordWithMostVowelsTxtBx.Text = findTheWordWithTheMostVowels(allWords);
                wordThatHadTheMostVowels = findTheWordWithTheMostVowels(allWords).ToCharArray();
                numOfVowelsTxtBx.Text = numberOfVowelsCounted(wordThatHadTheMostVowels).ToString();


                //This will create a file to write the stats to
                StreamWriter fileCreated = File.CreateText("TheStatisticsFile.txt");
                //rather than saving the information to a variable, this time it calls the methods directly and writes straight to the file. 
                fileCreated.WriteLine(findFirstWordAlphabetically(allWords));
                fileCreated.WriteLine(findLastWordAlphabetically(allWords));
                fileCreated.WriteLine(findTheLongestWord(allWords));
                fileCreated.WriteLine(findTheWordWithTheMostVowels(allWords) + " ,the count of this word was: " + numberOfVowelsCounted(wordThatHadTheMostVowels));
                fileCreated.Close();//close the writer now that the statistics have been written to.
            }
            else
            {
                MessageBox.Show("Chose not to select a File.");
            }

        }//ends open file btn

        /*
         * 
         * Here we begin a set of methods that will go through
         * the file that was read in. These methods will take in
         * the file text and sift through them for their specified 
         * return. These strings will be passed back to the button
         * method and then displayed to text boxes on the GUI.
         * 
         */
        public string findFirstWordAlphabetically(string[] allTheWords)
        {
            string theFirstWord = "";

            for(int i =0;i<allTheWords.Length; i++)
            {
                if(theFirstWord == "")
                {
                    theFirstWord = allTheWords[i];
                }else if(allTheWords[i].CompareTo(theFirstWord) < 0)
                {
                    theFirstWord = allTheWords[i];
                }//ends if else tree
            }//ends for
            return theFirstWord;
        }//ends find first word alphabetically

        public string findLastWordAlphabetically(string[] allTheWords)
        {
            string theLastWord = "";
            for (int i = 0; i < allTheWords.Length; i++)
            {
                if (theLastWord == "")
                {
                    theLastWord = allTheWords[i];
                }
                else if (allTheWords[i].CompareTo(theLastWord) > 0)
                {
                    theLastWord = allTheWords[i];
                }//ends if else tree
            }//ends for
            return theLastWord;
        }//ends find last word alphabetically

        public string findTheLongestWord(string[] allTheWords)
        {
            string theLongestWordInTheFile = "";
            for (int i = 0; i < allTheWords.Length; i++)
            {
                if (theLongestWordInTheFile == "")
                {
                    theLongestWordInTheFile = allTheWords[i];
                }else if(theLongestWordInTheFile.Length < allTheWords[i].Length)
                {
                    theLongestWordInTheFile = allTheWords[i];
                }//ends if else
            }//ends for loop
            return theLongestWordInTheFile;
        }//ends find the longest word

        public string findTheWordWithTheMostVowels(string[] allTheWords)
        {
            string wordWithTheMostVowels = "";
            int currentWordsVowelCount = 0;

            for (int i = 0; i < allTheWords.Length; i++)
            {
                char[] currentWordToChars;
                currentWordToChars = allTheWords[i].ToCharArray();

                if (i == 0)
                {
                    //call on a method that counts the number of vowels on this current word
                    currentWordsVowelCount = numberOfVowelsCounted(currentWordToChars);
                    wordWithTheMostVowels = allTheWords[i];
                } else if (currentWordsVowelCount < numberOfVowelsCounted(currentWordToChars)) { 
                    currentWordsVowelCount = numberOfVowelsCounted(currentWordToChars);
                    wordWithTheMostVowels = allTheWords[i];
                }//ends if else tree
                //if this if else is fallen out of the the word stored must have the highest vowel count.

            }//ends for loop

            return wordWithTheMostVowels;
        }//ends find the word with the most vowels

        public int numberOfVowelsCounted(char[] currentWord)
        {
            int numOfVowels = 0;
            for(int i = 0; i < currentWord.Length; i++)
            {
                if(currentWord[i] == 'a' || currentWord[i] == 'e' || currentWord[i] == 'i' || currentWord[i] == 'o' || currentWord[i] == 'u')
                {
                    numOfVowels++;
                }
            }
            return numOfVowels;
        }//end number of vowels

    }//ends class form
}//ends namespace
